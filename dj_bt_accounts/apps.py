from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "dj_bt_accounts"

    def ready(self):
        import_module("dj_bt_accounts.receivers")
